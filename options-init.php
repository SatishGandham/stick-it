<?php
global $sit_options_init;
$sit_options_init[] = array( 'name' => __( 'Stick it options', 'sit' ),
					'id' => 'stick-it',
					'type' => 'heading',
					'datatype' => 'none');

$sit_options_init[] = array( 'name' => __('CSS id/class of the section you want to stick', 'sit' ),
					'desc' => __(''),
					'id' => 'css_selector',
					'default' => '',
					'type' => 'text',
					'datatype' => 'text');

$sit_options_init[] = array( 'name' => __('Enable Use CSS', 'sit' ),
					'desc' => __('Check this box to enable user css'),
					'id' => 'enable_user_css',
					'default' => 'true',
					'type' => 'checkbox',
					'datatype' => 'bool');


$sit_options_init[] = array( 'name' => __('User CSS', 'sit' ),
					'desc' => __('Enter your custom CSS code here'),
					'id' => 'user_css',
					'default' => '',
					'type' => 'textarea',
					'datatype' => 'text');
