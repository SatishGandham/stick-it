=== Plugin Name ===
Contributors: 
Donate link: 
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag:
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily stick navigation menus to the top of browser window in any theme.

== Description ==
This plugin brings a useful feature to every WordPress theme that's usually present only in few premium themes.
Give the plugin the selector (class/id) of the navigation menu you want to stick and it's width and it will stick it
to the top of browser window making it easy for your readers to navigate your site.
== Installation ==


1. Upload the directory 'stick-it' to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Goto stick it options page and  enter the css selector for the navigation menu and its width (usually equal to the site width)

== Frequently Asked Questions ==


== Screenshots ==

1. Stick it !! admin options page.
2. This is how the navigation sticks to the browser window.

== Changelog ==

= 0.0.1 =
First official release