<?php
add_action( 'admin_menu', 'sit_add_admin_pages' );
function sit_add_admin_pages(){
	$sit_admin_page = add_menu_page( __( 'Stick it', 'sit' ), __( 'Stick it !!', 'sit' ), 'edit_theme_options', 'sit-admin-options', 'sit_admin_page', NULL, 12 );
	add_action( "admin_print_styles-$sit_admin_page", 'sit_admin_stylesheet' );
}
function sit_admin_stylesheet(){
	wp_enqueue_style( 'sit-admin-stylesheet', SIT_URL .'/css/style.css' );
}

function sit_admin_page(){
	?>
<div class="rs-wrap">
	<div id="header" class="clearfix">
		<h1 class="alignleft">
			<span style="color: #02b9e4">Stick</span> it<span style="color: #FFF">
				!</span>
		</h1>
		<div class="alignright">
			<em>version 0.0.1</em>
		</div>
	</div>
	<div class="alert alert-info" id="help">

		<ol>
			<li>Install <a
				href="https://addons.mozilla.org/en-US/firefox/addon/firebug/">firebug</a>
				addon for FF and open this page in FireFox.
			</li>
			<li>Right click on the element you want to stick and click inspect
				element. This opens firebug highlighting the code of the element you
				want to stick.</li>
			<li>Now find the <strong>div/ul/ol/nav</strong> enclosing your menu
				and note down its ID and class. On the right you have Style,
				Computed, Layout, DOM. Click on layout and note down the width.
			</li>
			<li>If it has an ID, your selector will (<strong>#</strong>) followed
				by the id name. If it has only class, then it will be (<strong>.</strong>)
				followed by the class name. Note: A div can have multiple classes,
				choose the one that is unique to the section.
			</li>
			<li>Sometimes the menu moves to the left, or flows to the right. To
				fix that, add <strong>selector{width:width you got in step
					2;margin:auto}</strong> to the user CSS
			</li>
		</ol>
		<div style="text-align: center">
			<h2>If you can not follow the above instructions we can install it
				for you for 15$</h2>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post"
				target="_top">
				<input type="hidden" name="cmd" value="_s-xclick"> <input
					type="hidden" name="hosted_button_id" value="35WNMCAN6FCK8"> <input
					type="image"
					src="https://www.paypalobjects.com/en_GB/i/btn/btn_paynowCC_LG.gif"
					border="0" name="submit"
					alt="PayPal � The safer, easier way to pay online."> <img alt=""
					border="0"
					src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1"
					height="1">
			</form>

		</div>
	</div>
	<form action="options.php" method="post" class="clearfix">
		<?php settings_fields('sit_options'); ?>

		<?php sit_options_input(); ?>
		<div class="submit clearfix">
			<input name="Submit" type="submit" class="button-primary alignright"
				value="<?php esc_attr_e('Save Changes'); ?>" />
		</div>
	</form>
</div>


<?php
}

function sit_options_init(){
	register_setting( 'sit_options', 'sit_options', 'sit_validate_options');
	add_settings_section( 'sit-options', 'Stick IT Settings', 'sit_header', 'sit-admin-options');
	add_settings_field( '','','sit_options_input','sit-admin-options', 'rs-options');
}
add_action( 'admin_init', 'sit_options_init');


function sit_validate_options($input){
	global $sit_options_init;
	$sanitized_options = NULL;
	foreach($sit_options_init as $opt){
		switch($opt['datatype']){
			case 'int':
				$sanitized_options[$opt['id']] = (int)$input[$opt['id']] ;
				break;

			case 'bool':
				$sanitized_options[$opt['id']] = (isset($input[$opt['id']]) && $input[$opt['id']] )? true  : false;
				break;

			case 'js':
				$sanitized_options[$opt['id']] = esc_js($input[$opt['id']]);
				break;

			case 'text':
				$sanitized_options[$opt['id']] = sanitize_text_field($input[$opt['id']]);
				break;

			case 'url':
				$sanitized_options[$opt['id']] = esc_url($input[$opt['id']]);
				break;

			default:
				break;
		}
	}
	return $sanitized_options;
	//Remember to change true to bool in checked case
}
function sit_options_input(){
	$sit_options = get_option('sit_options');
	$count= 0;
	global $sit_options_init;
	foreach($sit_options_init as $opt ){
		$class = ($count%2 == 1) ? 'even': 'odd';

		switch($opt['type']){
			case 'headig':
				echo '<h3>'.$opt['name'].'</h3>';
				break;

			case 'text':
				?>
<div class="text <?php echo $class;$count++?> clearfix">
	<label for="<?php echo $opt['id']?>"><h4>
			<?php echo $opt['name']?>
		</h4> <em><?php echo $opt['desc']?> </em> </label> <input type="text"
		name="sit_options[<?php echo $opt['id']?>]"
		id="<?php echo $opt['id']?>"
		value="<?php echo ($sit_options[$opt['id']])? $sit_options[$opt['id']] : $opt['default']?>" />
</div>
<?php
break;

case 'textarea':
	?>
<div class="textarea <?php echo $class;$count++?> clearfix">
	<label for="<?php echo $opt['id']?>"><h4>
			<?php echo $opt['name']?>
		</h4> </label>
	<textarea rows="7" cols="50"
		name="sit_options[<?php echo $opt['id']?>]"
		id="<?php echo $opt['id']?>">
		<?php if ( isset( $sit_options[ $opt['id'] ] ) && $sit_options[ $opt['id'] ] != '' ) {
			echo stripslashes( esc_attr( $sit_options[ $opt['id'] ] ) );
		} else { echo stripslashes($opt['default']);
} ?>
	</textarea>
	<br> <label for="<?php echo $opt['id']?>"><em><?php echo $opt['desc']?>
	</em> </label>
</div>
<?php 		break;
case 'checkbox':
	?>
<div
	class="checkbox <?php echo $class;$count++?> clearfix">
	<!--<h4><?php echo $opt['name']?></h4>-->
	<input type="checkbox" name="sit_options[<?php echo $opt['id']?>]"
		value="true" <?php checked( $sit_options[ $opt['id'] ], true ); ?>
		id="<?php echo $opt['id']; ?>" /> <label for="<?php echo $opt['id']?>"><em><?php echo $opt['desc']?>
	</em> </label>
</div>
<?php
break;
		}

	}
}

