<?php
/*
 Plugin Name: Stick it !!
Plugin URI: http://askwebexpert.com/stick-it
Description: Easily stick navigation menus to the top of browser window in any theme.
Version: 0.0.1
Author: Satish Gandham
Author URI: http://AskWebEXpert.Com
License: GPL2
*/
define('SIT_PATH',  dirname(__FILE__));
define('SIT_URL',plugin_dir_url(__FILE__) );
include SIT_PATH.'/admin-page.php';
include SIT_PATH.'/options-init.php';

GLOBAL $sit_options;
$sit_options = get_option('sit_options');

add_action('init','sit_load_scripts');
function sit_load_scripts(){
	wp_enqueue_script('jquery');
	wp_enqueue_script('sticky',SIT_URL.'/js/jquery.sticky.js',array('jquery'),'0.0',TRUE);
}

add_action('wp_footer','sit_activate');

function sit_activate(){
	GLOBAL $sit_options;

	?>
<script>
  jQuery(document).ready(function(){
	  jQuery("<?php echo $sit_options['css_selector']?>").sticky({topSpacing:0});
  });
</script>
<?php
}

add_action('wp_head','sit_css');
function sit_css(){
GLOBAL $sit_options;

?>
<style type="text/css">
<?php echo $sit_options['user_css'] ?>
</style>
<?php
}
?>